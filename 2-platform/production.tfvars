# remote state
remote_state_key = "PROD/infrastructure.tfstate"
remote_state_bucket = "cc-ecs-fargate-terraform-remote-state-2"

ecs_domain_name = "npstaging.coltradedigital.co"
ecs_cluster_name = "Production-ECS-Cluster"
internet_cidr_blocks = "0.0.0.0/0"